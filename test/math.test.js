const { calculateTip, fahrenheitToCelsius, celsiusToFahrenheit } = require('../src/math');

test('Test Hello', () => {
    expect('Hello').toMatch('Hello');
}) 

test('test calculateTip and return total with tip', () => {
    const total = 10;
    const tip = 20;
    const result = calculateTip(total, tip);
    expect(result).toBeGreaterThan(10);
    expect(result).toBeLessThan(15);
    expect(result).toBe(12); 
})

test('test should convert 32 F to 0 c', () => {
    const temp = 32;
    const result = fahrenheitToCelsius(temp);
    expect(result).toBeGreaterThanOrEqual(0);
    expect(result).toBeLessThanOrEqual(0);
    expect(result).toBe(0);
})

test('test should convert 0 C to 32 F',() => {
    const temp = 0;
    const result = celsiusToFahrenheit(temp);
    expect(result).toBe(32);
})

// test(`Async Test Demo`, (done) => {
//     setTimeout(() => {
//         expect(1).toBe(1);
//         done();
//     }, 2000);
// })