const request = require('supertest');
const app = require('../src/app');
const randomEmail = require('random-email');

// testing test
describe(`Testing api`, () => {
  it(`test own testing`, () => {
    expect(true).toBe(true);
  })
})

describe('GET /', function () {
  it('responds status 200 with Welcome to my API', async () => {
    await request(app)
      .get('/')
      .expect(200);
  });
});

// describe('GET /users', () => {
//     it(`get user list response json array`,async () => {
//         await request(app)
//           .get('/users')
//           .set('Accept', 'application/json')
//           .expect('Content-Type', /json/)
//           .expect(200);
//     })
// })


describe('GET /users', () => {
  it(`get user list response json array`, (done) => {
    request(app)
      .get('/users')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done);
  })
})

describe('GET /user by Id', () => {
  it(`get user by user id and response json`, (done) => {
    request(app)
      .get('/user')
      .send({
        id: '5e56394a4009202c74a3eae6'
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done);
  })
})

// using ramdom email to create user
describe('POST /user', () => {
  const demoEmail = randomEmail({
    domain: 'example.com'
  });
  it(`add new user and response json`, (done) => {
    request(app)
      .post('/user')
      .send({
        name: "Leo Dary",
        email: demoEmail,
        password: "abc123"
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        done();
      });
  })
})

describe(`PUT /user`, () => {
  const demoEmail = randomEmail({
    domain: 'example.com'
  });
  it(`update user and reponse json`, (done) => {
    request(app)
      .put('/user')
      .send({
        id: "5e61bb8d69f66a493cfcc67d",
        name: "Demo Updated",
        email: demoEmail,
        password: "updated123"
      })
      .set(`Accept`, `application/json`)
      .expect(`Content-Type`, /json/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        done();
      })
  })
})