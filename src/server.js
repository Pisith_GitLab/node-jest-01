const app = require('./app');

app.listen(process.env.NODE_PORT || 3000, () => console.log(`Server is running on port 3000`));