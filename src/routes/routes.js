const express = require('express');

const UserController = require('../controllers/user-controller');
const TaskController = require('../controllers/task-controller');

const routes = new express.Router();
// Homepage endpoint
routes.get('/', (req, res) => {
    res.status(200).send(`Welcome to my API`);
})
// User endpoint
routes.get('/users', UserController.findMany);
routes.post('/user', UserController.store);
routes.get('/user', UserController.findById);
routes.put('/user', UserController.updateById);
routes.delete('/user', UserController.remove);
// Task endpoint
routes.get('/tasks', TaskController.findMany);
routes.get('/task', TaskController.findById);
routes.post('/task', TaskController.store);
routes.delete('/task', TaskController.remove);

module.exports = routes;