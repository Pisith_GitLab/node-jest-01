const User = require('../models/user');

class UserController {
    async store(req, res) {
        try {
            const user = new User({
                name: req.body.name,
                email: req.body.email,
                password: req.body.password
            });
            await user.save();
            return res.status(200).json({
                id: user._id,
                name: user.name,
                email: user.email
            });
        } catch (err) {
            return res.status(500).json({ error: 'ម៉ាស៊ីនមេមានបញ្ហាហើយ'});
        }
    }
    async findById(req, res) {
        try {
            const user = await User.findById(req.body.id);
            if(user) {
                return res.status(200).json(user);
            } else {
                return res.status(404).json({ message: 'អត់មានទេ'});
            }
        } catch (err) {
            return res.status(500).json({ error: 'ម៉ាស៊ីនមេមានបញ្ហាហើយ'});
        }
    }
    async findMany(req, res) {
        try {
            const userList = await User.find({});
            if(userList) {
                return res.status(200).json(userList);
            } else {
                return res.status(404).json({ message: 'អត់មានទេ'});
            }
        } catch (err) {
            return res.status(500).json({ error: 'ម៉ាស៊ីនមេមានបញ្ហាហើយ'});
        }
    }
    async updateById(req, res) {
        try {
            const query = {_id: req.body.id};
            const updateData = {
                $set: {
                    name: req.body.name,
                    email: req.body.email,
                    password: req.body.password
                }
            };
            const result = await User.updateOne(query, updateData, { runValidators: true });
            if (result) {
                return res.status(200).json({
                    id: req.body.id,
                    name: req.body.name,
                    email: req.body.email
                });
            } else {
                return res.status(404).json({message: `អត់មានទេ`});
            }
        } catch(err) {
            return res.status(500).json({ error: 'ម៉ាស៊ីនមេមានបញ្ហាហើយ'});
        }
    }
    async remove(req, res) {
        try {
            const user = await User.findById(req.body.id);
            if (user) {
                await user.remove();
                return res.status(200).json({message: 'លុបចេញរូចរាល់'});
            } else {
                return res.status(404).json({ message: 'អត់មានទេ'});
            }
        } catch (err) {
            return res.status(500).json({ error: 'អត់មានទេ'});
        }
    }
}

module.exports = new UserController();
