const Task = require('../models/task');

class TaskController {
    async store(req, res) {
        try {
            const task = new Task({
                title: req.body.title,
                description: req.body.description
            });
            await task.save();
            return res.status(200).json({
                id: task._id,
                title: task.title,
                description: task.description
            });
        } catch (err) {
            return res.status(500).json({ error: 'ម៉ាស៊ីនមេមានបញ្ហាហើយ'});
        }
    }
    async findById(req, res) {
        try {
            const task = await Task.findById(req.body.id);
            if (task) {
                return res.status(200).json(task);
            } else {
                return res.status(404).json({ message: 'អត់មានទេ'});
            }
        } catch (err) {
            return res.status(500).json({ error: 'ម៉ាស៊ីនមេមានបញ្ហាហើយ'});
        } 
    }
    async findMany(req, res) {
        try {
            const taskList = await Task.find({});
            if (taskList) {
                return res.status(200).json(taskList);
            } else {
                return res.status(404).json({ message: 'អត់មានទេ'});
            }
        } catch (err) {
            return res.status(500).json({ error: 'ម៉ាស៊ីនមេមានបញ្ហាហើយ'});
        }
    }
    async remove(req, res) {
        try {
            const task = await Task.findById(req.body.id);
            if (task) {
                await task.remove();
                return res.status(200).json({message: 'លុបចេញរូចរាល់'});
            } else {
                return res.status(404).json({ message: 'អត់មានទេ'});
            }
        } catch (err) {
            return res.status(500).json({ error: 'អត់មានទេ'});
        }
    }
}

module.exports = new TaskController();